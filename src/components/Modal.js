import React from 'react';
import './Modal.scss';

const Modal = ({ header, closeButton, text, actions, onClose, backgroundColor }) => {
    const styleModal = {
        width: '500px',
        height: '200px',
        position: 'fixed',
        top: '35%',
        left: '30%',
        display: 'flex',
        flexDirection: 'column',
        borderRadius: '8px',
        color: 'white',
        zIndex: '1',
        backgroundColor: backgroundColor,
    };

    const overlayStyle = {
        position: 'fixed',
        top: '0',
        left: '0',
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        zIndex: '2',
    };

    const handleOverlayClick = (event) => {
        if (event.target === event.currentTarget) {
            onClose();
        }
    };

    return (
        <div>
            <div style={overlayStyle} onClick={handleOverlayClick}>
                <div style={styleModal}>
                    <div className="header-wrapper">
                        <h2>{header}</h2>
                        {closeButton && <span className="close-button" onClick={onClose}>×</span>}
                    </div>
                    <p>{text}</p>
                    <div className="modal-actions">{actions}</div>
                </div>
            </div>
        </div>
    );
};

export default Modal;
