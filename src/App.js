import React, { useState } from 'react';
import Button from './components/Button';
import Modal from './components/Modal';
import './App.css';

const App = () => {
    const [isModalOpen, setModalOpen] = useState(false);
    const [modalType, setModalType] = useState('');

    const openModal = (type) => {
        setModalType(type);
        setModalOpen(true);
    };

    const closeModal = () => {
        setModalOpen(false);
        setModalType('');
    };

    const handleOk = () => {
        closeModal();
    };

    const handleCancel = () => {
        closeModal();
    };

    return (
        <div className="App">
            <div className='btn-wrapper' >
            <Button backgroundColor="red" text="Delete" onClick={() => openModal('delete')} />
            <Button backgroundColor="green" text="Save" onClick={() => openModal('save')} />
            </div>
            {isModalOpen && (
                modalType === 'delete' ? (
                    <Modal
                        backgroundColor='red'
                        header="Do you want delete this file ?"
                        closeButton={true}
                        text={(
                            <>
                                Once you delete this file, if you want to undo this action.<br />
                                Are you sure you want to delete it?
                                <div className="modal-button-container">
                                    <button className='custom-button' onClick={handleOk}>Delete</button>
                                    <button className='custom-button' onClick={handleCancel}>Cancel</button>
                                </div>
                            </>

                        )}

                        onClose={closeModal}

                    />
                ) : (
                    <Modal
                        backgroundColor='green'
                        header="You want save this file ?"
                        closeButton={true}
                        text={(
                            <>
                                Click to Save for accept action.<br />
                                or Cancel for decline
                                <div className="modal-button-container">
                                    <button className='custom-button' onClick={handleOk}>Save</button>
                                    <button className='custom-button' onClick={handleCancel}>Cancel</button>
                                </div>
                            </>
                        )}
                        onClose={closeModal}
                    />
                )
            )}
        </div>
    );
};

export default App;
